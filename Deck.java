import java.util.Random;

public class Deck {
    private Card[] stack;
    private int numberOfCards;
    private Random rng = new Random();

    // Constructor
    public Deck() {
        String[] suits = new String[]{"Hearts", "Diamonds", "Clubs", "Spades"};
        int[] ranks = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

        // Create array of fresh deck
        this.stack = new Card[52];
        this.numberOfCards = this.stack.length;

        int index = 0;
        for (String s : suits) {
            for (int r : ranks) {
                stack[index] = new Card(s, r);
                index++; 
            }
        }
        System.out.println("New deck created!\n");
    }

    // Getters
    public int length() {
        return this.numberOfCards;
    }

    // Methods
    public Card drawTopCard() {
        this.numberOfCards -= 1;
        return stack[this.numberOfCards];
    }

    public void shuffle() {
        Card placeholder;

        for (int i = 0; i < numberOfCards; i++) {
            int newSpot = this.rng.nextInt(numberOfCards);
            placeholder = stack[newSpot];
            stack[newSpot] = stack[i];
            stack[i] = placeholder;
        }
        System.out.println("All cards shuffled!\n");
    }

    // toString
    public String toString() {
        String fullStack = "Current deck:\n";

        for (int i = 0; i < this.numberOfCards; i++) {
            fullStack += stack[i].toString() + "\n";
        }
        fullStack += "\n";
        
        return fullStack;
    }
}