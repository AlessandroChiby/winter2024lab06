public class GameManager {
    private Deck drawPile;
    private Card centerCard;
    private Card playerCard;

    // Constructor
    public GameManager() {
        drawPile = new Deck();
        drawPile.shuffle();
        centerCard = drawPile.drawTopCard();
        playerCard = drawPile.drawTopCard();
    }

    // Getters
    public int getNumberOfCards() {
        return drawPile.length();
    }

    // Methods
    public void dealCards() {
        centerCard = drawPile.drawTopCard();
        playerCard = drawPile.drawTopCard();
    }

    public int calculatePoints() {
        if (centerCard.getRank() == playerCard.getRank()) {
            return 4;
        } else if (centerCard.getSuit() == playerCard.getSuit()) {
            return 2;
        } else {
            return -1;
        }
    }
    
    // toString
    public String toString() {
        return "----------------------------------\nCenter card: " + centerCard + "\nPlayer card: " + playerCard + "\n----------------------------------";
    }
}