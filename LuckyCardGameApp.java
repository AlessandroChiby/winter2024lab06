import java.util.Scanner;

public class LuckyCardGameApp {
    public static void main(String[] args) {
        GameManager manager = new GameManager();
        int totalPoints = 0;
        int round = 0;

        System.out.println("Welcome!");
        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
            round++;
            System.out.println("**********************************\nRound: " + round);
            System.out.println(manager);
            totalPoints += manager.calculatePoints();
            System.out.println("Current points: " + totalPoints);
            manager.dealCards();
        }
        if (totalPoints >= 5) {
            System.out.println("Player wins with " + totalPoints + " points after " + round + " rounds!");
        } else {
            System.out.println("Player loses with " + totalPoints + " points!");
        }

    }
}